<?php
include 'assets/main.class.php';
$main	 = new main();
$act	 = isset($_REQUEST['act']) ? $_REQUEST['act'] : '';

switch ($act) {
	case 'setCategory':
		if (!empty($_REQUEST['category_new'])) {
			$category_old	 = !empty($_REQUEST['category_old']) ? $_REQUEST['category_old'] : null;
			$category_new	 = $_REQUEST['category_new'];
			$main->setCategory($category_new, $category_old);
			return 1;
		}
		break;
	case 'setBookmark':
		if (!empty($_REQUEST['category']) AND ! empty($_REQUEST['arr_bookmark'])) {
			$category		 = $_REQUEST['category'];
			$arr_bookmark	 = $_REQUEST['arr_bookmark'];
//			var_dump($_REQUEST);
			$main->setBookmark($category, $arr_bookmark);
		}
		break;
	case 'setTestData':
		$main->set(array(
			'news'	 => array(
				array('name' => 'putin', 'url' => 'bla.ru'),
				array('name' => 'putin1', 'url' => 'bla.ru'),
				array('name' => 'putin2', 'url' => 'bla.ru'),
				array('name' => 'putin3', 'url' => 'bla.ru'),
			),
			'news2'	 => array(
				array('name' => 'putin', 'url' => 'bla.ru'),
				array('name' => 'putin1', 'url' => 'bla.ru'),
				array('name' => 'putin2', 'url' => 'bla.ru'),
				array('name' => 'putin3', 'url' => 'bla.ru'),
			)
		));
		break;
	case 'deleteBookmark':

		if (!empty($_REQUEST['category'])) {
			$category	 = $_REQUEST['category'];
			$bookmark_id = empty($_REQUEST['bookmark_id'])?0:$_REQUEST['bookmark_id'];
			$main->deleteBookmark($category, $bookmark_id);
		}
		break;
	case 'deleteCategory':
		if (!empty($_REQUEST['category_name'])) {
			$category = $_REQUEST['category_name'];
			$main->deleteCategory($category);
		}
		break;
	default :
		$data = $main->get();
		$main->render('assets/view', array('data' => $data));
		break;
}
?>
