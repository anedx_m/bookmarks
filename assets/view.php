<script src="assets/jquery-2.1.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/theme.css">
<table>
	<?php
	$categories = array();
	foreach ($data as $category => $bookmarks) {
		$categories[]	 = $category;
		$delete			 = count($bookmarks) > 0 ? false : true;
		?>
		<tr class="category">
			<td class="category-name" ><?php echo $category ?></td>
			<td class="actions" ><div><?php if ($delete) { ?><a href="" class="delete">delete</a><?php }; ?></div></td>
		</tr>
		<?php
		$id = 0;
		foreach ($bookmarks as $id=>$bookmark) {
			$bookmark = (array) $bookmark;
			?>
			<tr class="bookmark" data-category="<?php echo $category ?>" data-id="<?php echo $id ?>">
				<td class="name"><a href="<?php echo $bookmark['url'] ?>" target=_blank><?php echo $bookmark['name'] ?></td>
				<td class="actions">
					<input class="bookmark-url" type="hidden" value="<?php echo $bookmark['url'] ?>">
					<a href="" class="edit">edit</a>
					<a href="" class="delete">delete</a>
				</td>
			</tr>
			<?php
		}
	}
	?>
</table>
<br>
<div class="edit-category">
	<label for="input-category-name">Category name: </label>
	<input class="input-category-name-old" type="hidden">
	<input class="input-category-name">
	<input class="save-category-name" type="button" value="Save Category">
</div>
<div class="edit-bookmark">
	<label for="input-bookmark-name">Bookmark name: </label>
	<input class="input-bookmark-name">
	<label for="bookmark-url">URL: </label>
	<input class="input-bookmark-url" value="http://">
	<label for="bookmark-category">Category: </label>
	<select  class="book-category">
		<?php
		foreach ($categories as $category) {
			?>
			<option data-category="<?php echo $category ?>"><?php echo $category ?></option>
			<?php
		}
		?>
	</select>
	<input class="save-bookmark" type = "button" value = "Save Bookmark">
</div>
<div class="info"></div>
<script>
	$( '.category .delete' ).click( function() {
		if(!confirm("Are you sure?")) 
			return false;
			
		var category_name = $( this ).closest( '.category' ).find('.category-name').html()
		$.post( 'index.php?act=deleteCategory', 'category_name=' + category_name, function( data ) {
			document.location.reload()
//			$( '.info' ).html( data )
		} )
		return false;
	} )
	$( '.bookmark .edit' ).click( function() {
		var id = $( this ).closest( '.bookmark' ).attr( 'data-id' )
		var name = $( this ).closest( '.bookmark' ).find( '.name a' ).html()
		var url = $( this ).closest( '.actions' ).find( '.bookmark-url' ).val()
		var category = $( this ).closest( '.bookmark' ).attr( 'data-category' )
		$( '.input-bookmark-name' ).attr( 'data-id', '' );
		$( '.input-bookmark-name' ).attr( 'data-id', id );
		$( '.input-bookmark-name' ).val( name );
		$( '.input-bookmark-url' ).val( url );
		$( '.book-category option' ).removeAttr( 'selected' )
		$( '.book-category option[data-category=' + category + ']' ).attr( 'selected', 1 )
		return false;
	} )
	$( '.bookmark .delete' ).click( function() {
		if(!confirm("Are you sure?")) 
			return false;
		var category = $( this ).closest( '.bookmark' ).attr( 'data-category' )
		var id = $( this ).closest( '.bookmark' ).attr( 'data-id' )

		var request = 'category=' + category + '&bookmark_id=' + id
//		console.log( request )
		$.post( 'index.php?act=deleteBookmark', request, function( data ) {
			document.location.reload()
		} )
		return false;
	} )
	$( '.save-bookmark' ).click( function() {
		var category = $( '.book-category' ).val()
		var bookmark = { arr_bookmark: {
				id: $( '.input-bookmark-name' ).attr( 'data-id' ),
				name: $( '.input-bookmark-name' ).val(),
				url: $( '.input-bookmark-url' ).val() }
		}
		var request = 'category=' + category + '&' + $.param( bookmark )
		console.log( request )
		$.post( 'index.php?act=setBookmark', request, function( data ) {
//			$( '.info' ).html( data )
			document.location.reload()
		} )
	} )
	$( '.save-category-name' ).click( function() {
		var category_name = $( '.input-category-name' ).val()
		if(!category_name)
			return false;
		$.post( 'index.php?act=setCategory', 'category_new=' + category_name, function( data ) {
			document.location.reload()
		} )
		return false;
	} )
</script>

