<?php

class main {

	const file = 'data.json';

	public function get() {
		if(!file_exists(self::file))
			return array();
		$data = json_decode(file_get_contents(self::file), true); //as array
		return $data;
	}

	public function set($data) {
		return file_put_contents(self::file, json_encode($data));
	}

	public function setBookmark($category, $arr_bookmark) {
		$data = $this->get();
		//var_dump($arr_bookmark['id']);
		if ($arr_bookmark['id']!=="") {
			$id						 = $arr_bookmark['id'];
			unset($arr_bookmark['id']);
			$data[$category][$id]	 = $arr_bookmark;
		} else {
			unset($arr_bookmark['id']);
			$data[$category][] = $arr_bookmark;
		}
		// bookmark updated 
		usort($data[$category],function($a, $b){
			return $a['name']>$b['name']?1:-1;
		});
		//print_r($data[$category]);die();
		$this->set($data);
	}

	public function deleteBookmark($category, $bookmark_id) {
		$data = $this->get();
		unset($data[$category][$bookmark_id]);  
		$this->set($data);
	}

	public function setCategory($category_new, $category_old = null) {
		$data = $this->get();
		if ($category_old) {
			$data[$category_new] = $data[$category_old];
			uset($data[$category_old]);
		} else {
			$data[$category_new] = array();
		}
		ksort($data);
		$this->set($data);
	}

	public function deleteCategory($category_name) {
		$data = $this->get();
		unset($data[$category_name]);
		$this->set($data);
	}

	public function render($view, $param = array()) {
		if (!is_null($param)) {
			extract($param);
		}

		include "$view.php";
	}

}
